import json
import logging
import re

import flask
from apiclient.discovery import build
from flask import Flask, jsonify
import uuid
from oauth2client.contrib.flask_util import UserOAuth2

from config import CALENDAR_ID

app = Flask(__name__)

# app.config['SECRET_KEY'] = 'your-secret-key'
app.config['GOOGLE_OAUTH2_CLIENT_SECRETS_FILE'] = 'client_secrets.json'

# # or, specify the client id and secret separately
# app.config['GOOGLE_OAUTH2_CLIENT_ID'] = 'your-client-id'
# app.config['GOOGLE_OAUTH2_CLIENT_SECRET'] = 'your-client-secret'

oauth2 = UserOAuth2(app)
# oauth2 = UserOAuth2(app, include_granted_scopes=True) # include_granted_scopes is already not support.
app.secret_key = str(uuid.uuid4())

@app.route('/')
def hello():
    return 'Calendar ID: {}'.format(CALENDAR_ID)


# Note that app.route should be the outermost decorator.
@app.route('/needs_credentials')
@oauth2.required
def example():
    # http is authorized with the user's credentials and can be used
    # to make http calls.
    http = oauth2.http()

    # Or, you can access the credentials directly
    credentials = oauth2.credentials


@app.route('/info')
@oauth2.required
def info():
    return "Hello, {} ({})".format(oauth2.email, oauth2.user_id)


@app.route('/login')
def login():
    return oauth2.authorize_url("/")


@app.route('/calendar')
# @oauth2.required()
@oauth2.required(scopes=['https://www.googleapis.com/auth/calendar'])
def requires_calendar():

    # http = oauth2.http()
    if oauth2.has_credentials():
        logging.info("has credentilas")
        service = build('calendar', 'v3', oauth2.http())
        calendars = service.calendarList().list().execute()
        logging.info("#### ")
        logging.info(json.dumps(calendars))
        return jsonify(calendars)

    else:
        return 'No credentials!'


@app.route('/event/insert-batch')
@oauth2.required(scopes=['https://www.googleapis.com/auth/calendar'])
def event_insert():

    # http = oauth2.http()
    if oauth2.has_credentials():
        logging.info("has credentilas")
        service = build('calendar', 'v3', oauth2.http())
        batch = service.new_batch_http_request(callback=event_insert_callback)
        for v in range(5):
            title = 'Sample {}'.format(v)
            event = create_event_json(title)
            # service.events().insert(calendarId=CALENDAR_ID, body=event).execute()
            batch.add(service.events().insert(calendarId=CALENDAR_ID, body=event))
        batch.execute()
        return jsonify({'message': 'start batch'})

    else:
        return 'No credentials!'


def create_event_json(title):
    event = {
        'summary': title,
        'location': '800 Howard St., San Francisco, CA 94103',
        'description': 'A chance to hear more about Google\'s developer products.',
        'start': {
            'dateTime': '2017-07-28T09:00:00-07:00',
            'timeZone': 'America/Los_Angeles',
        },
        'end': {
            'dateTime': '2017-07-28T17:00:00-07:00',
            'timeZone': 'America/Los_Angeles',
        },
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=2'
        ],
        'attendees': [
            {'email': 'lpage@example.com'},
            {'email': 'sbrin@example.com'},
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10},
            ],
        },
    }
    return event


@app.route('/event/delete-batch')
@oauth2.required(scopes=['https://www.googleapis.com/auth/calendar'])
def evet_delete():
    date_from = '2017-07-28T09:00:00Z'
    date_to = '2017-07-30T17:00:00Z'

    service = build('calendar', 'v3', oauth2.http())
    events = service.events().list(
        calendarId=CALENDAR_ID, timeMin=date_from, timeMax=date_to,
        singleEvents=True, pageToken=None,
        orderBy='startTime').execute()

    target_events = []
    batch = service.new_batch_http_request(callback=event_delete_callback)
    for event in events['items']:
        if re.search('Sample', event['summary']):
            batch.add(service.events().delete(calendarId=CALENDAR_ID, eventId=event['id']))
            target = { 'id': event['id'], 'summary': event['summary']}
            target_events.append(target)
    batch.execute()
    return jsonify({'events': target_events})


def event_insert_callback(request_id, response, exception):
    if exception is not None:
        # Do something with the exception
        logging.error('Error!!')
        logging.exception(exception)
    else:
        # Do something with the response
        logging.info('Succeed in event insert')
        logging.info('request_id: {}'.format(request_id))
        logging.info('response: {}'.format(response))


def event_delete_callback(request_id, response, exception):
    if exception is not None:
        # Do something with the exception
        logging.error('Error!!')
        logging.exception(exception)
    else:
        # Do something with the response
        logging.info('Succeed in event delete')
        logging.info('request_id: {}'.format(request_id))
        logging.info('response: {}'.format(response))

@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception(e)
    return 'An internal error occurred.', 500
